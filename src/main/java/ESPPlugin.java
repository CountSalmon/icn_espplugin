import de.lordsalmon.dev.icn.backend.api.IcnAPI;
import de.lordsalmon.dev.icn.backend.api.config.ConfigService;
import de.lordsalmon.dev.icn.backend.api.config.DataService;
import de.lordsalmon.dev.icn.backend.api.messaging.HttpRequest;
import de.lordsalmon.dev.icn.backend.api.utils.CallValidator;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ESPPlugin extends IcnAPI {

    public static final String PLUGIN_VERSION = "1.0.1";
    public static final String LATEST_MESSAGE = "now chained accessors";
    public static final String DISPLAY_NAME = "ESP - LED";

    public IcnAPI ICNInstance = IcnAPI.getInstance(ESPPlugin.class);

    public void initialize() {
        ICNInstance.setConfigService(ConfigService.getInstance(ESPPlugin.class))
                .setLogger(Logger.getLogger(ESPPlugin.class.getSimpleName()))
                .setDataService(DataService.getInstance(ESPPlugin.class));
    }

    /**
     * method to interact with a specific component
     *
     * @param object
     */
    public void call(JSONObject object, JSONObject componentBase) {
        ICNInstance.getLogger().log(Level.INFO, String.format("Call recieved: %s", object.toString()));
        if (isValid(object)) {
            String ip = componentBase.getString("ip");
            Map<String, String> parameters = new HashMap<>();
            String type = object.getString("type");
            switch (type) {
                case "led":
                    parameters.put("red", String.valueOf(object.getInt("red")));
                    parameters.put("green", String.valueOf(object.getInt("green")));
                    parameters.put("blue", String.valueOf(object.getInt("blue")));
                    break;
                case "random":
                    parameters.put("delay", String.valueOf(object.getInt("delay")));
                case "off":
                case "totaly/random":
                case "random/constant":
                default:
                    break;
            }
            HttpRequest httpRequest = null;
            try {
                httpRequest = new HttpRequest(String.format("http://%s/%s", ip, type))
                        .setMethod("GET")
                        .setParameters(parameters);
                httpRequest.execute();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            ICNInstance.getLogger().log(Level.OFF, "The given given object is not valid!");
        }
    }

    /**
     * validate the recieved JSONObject by setting
     *
     * @param object
     * @return
     */
    private boolean isValid(JSONObject object) {
        CallValidator callValidator = new CallValidator();
        List<String> callValidatorList = new ArrayList<String>();
        callValidatorList.add("type");
        callValidator.start(object);
        if (callValidator.isValid()) {
            CallValidator typeValidator = new CallValidator();
            List<String> typeVList = new ArrayList<>();
            switch (object.getString("type")) {
                case "random":
                    typeVList.add("delay");
                    break;
                case "random/constant":
                case "led":
                    typeVList.add("red");
                    typeVList.add("green");
                    typeVList.add("blue");
                    break;
                case "totaly/random":
                default:
                    break;
            }
            typeValidator.setValidationList(typeVList);
            typeValidator.start(object);
            return typeValidator.isValid();
        }
        return false;
    }
}
